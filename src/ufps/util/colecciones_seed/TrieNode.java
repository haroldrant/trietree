/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

/**
 *
 * @author Javier
 */
public class TrieNode implements Comparable{
    private String info;
    private TrieNode padre;
    private ListaCD<TrieNode> hijos;
    
    /**
     * Constructor con parametros de la clase TrieNode
     * @param info
     * @param p
     */
    public TrieNode(String info, TrieNode p){
        this.padre = p;
        this.info = info;
        this.hijos = new ListaCD<>();
    }
    
    /**
     * Método que realiza la búsqueda de un hijo según la info
     * @param info
     * @return TrieNode si lo encuentra, null en caso contrario
     */
    public TrieNode getHijo(String info) {
        if(!this.hijos.esVacia()){
            for(TrieNode hijo: this.hijos){
                if(hijo.info.equals(info))
                    return hijo;
            }
        }
        return null;
    }

    public TrieNode getPadre() {
        return padre;
    }

    public String getInfo() {
        return info;
    }

    public ListaCD<TrieNode> getHijos() {
        return hijos;
    }

    @Override
    public boolean equals(Object obj) {
        String other = (String) obj;        
        return this.info.equals(other);
    }
    
    @Override
    public int compareTo(Object obj){
        TrieNode other = (TrieNode)obj;
        return this.info.compareTo(other.getInfo());
    }
}
