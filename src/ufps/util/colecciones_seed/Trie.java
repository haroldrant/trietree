/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import java.util.ArrayList;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author Harold
 */
public class Trie {

    private TrieNode raiz = new TrieNode("", null);
    private String[] l = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "Ñ", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    private ArrayList rep = new ArrayList();

    /**
     * Constructor de la clase trie
     *
     * @param url
     */
    public Trie(String url) {
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object[] texto = archivo.leerArchivo();
        String aux = texto[0].toString();
        String[] datos = aux.split(",");
        for (String palabra : datos) {
            palabra = validarEspacio(palabra);
            this.insertar(palabra);
        }
    }

    private String validarEspacio(String palabra) {
        String msg = "";
        String[] part = palabra.split("");
        for (int i = 0; i < part.length; i++) {
            if (part[i].equals(" ")) {
                msg += part[++i];
            } else {
                msg += part[i];
            }
        }
        return msg;
    }

    /**
     * Método que inserta una nueva palabra en el Trie
     *
     * @param palabra
     */
    public void insertar(String palabra) {
        palabra = palabra.toUpperCase();
        palabra = this.tildes(palabra);
        ListaCD<String> palabraDividida = this.dividir(palabra);
        TrieNode actual = this.raiz;
        for (String l : palabraDividida) {
            TrieNode aux = actual.getHijo(l);
            if (aux != null) {
                actual = aux;
            } else {
                actual.getHijos().insertarOrdenado(new TrieNode(l, actual));
                actual = actual.getHijo(l);
            }
        }
    }

    /**
     * Método que determina si una palabra se encuentra en el Trie,
     *
     * @param palabra
     * @return true si la encuentra, false en caso contrario.
     */
    public boolean buscar(String palabra) {
        palabra = palabra.toUpperCase();
        palabra = this.tildes(palabra);
        ListaCD<String> palabraDividida = this.dividir(palabra);
        TrieNode actual = this.raiz;
        for (String l : palabraDividida) {
            TrieNode aux = actual.getHijo(l);
            actual = aux;
            if (aux == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * Método que divide una cadena de texto. Retorna una ListaCD
     */
    private ListaCD<String> dividir(String palabra) {
        ListaCD<String> palabraDividida = new ListaCD<>();
        String[] split = palabra.split("");
        for (int i = 0; i < split.length; i++) {
            if (i < split.length - 2) {
                String digrafo = split[i] + split[i + 1];
                if (esDigrafo(digrafo, split[i + 2])) {
                    palabraDividida.insertarAlFinal(digrafo);
                    i++;
                } else {
                    palabraDividida.insertarAlFinal(split[i]);
                }
            } else {
                palabraDividida.insertarAlFinal(split[i]);
            }
        }
        return palabraDividida;
    }

    /**
     * Método que determina si dos letras son dígrafo. Retorna true si lo
     * cumple, de lo contrario false.
     */
    private boolean esDigrafo(String palabra, String vocal) {
        if (vocal.equals("E") || vocal.equals("I")) {
            if (palabra.equals("QU") || palabra.equals("GU")) {
                return true;
            }
        }
        return palabra.equals("CH") || palabra.equals("LL") || palabra.equals("RR");
    }

    /**
     * Método que reemplaza las tildes en una palabra. Retorna el string sin
     * tildes.
     */
    private String tildes(String palabra) {
        String[] split = palabra.split("");
        palabra = "";
        for (int i = 0; i < split.length; i++) {
            switch (split[i]) {
                case "Á":
                    split[i] = "A";
                    break;
                case "É":
                    split[i] = "E";
                    break;
                case "Í":
                    split[i] = "I";
                    break;
                case "Ó":
                    split[i] = "O";
                    break;
                case "Ú":
                    split[i] = "U";
                    break;
            }
            palabra += split[i];
        }
        return palabra;
    }

    public String letraRepetida(TrieNode raiz, int busca) {
        int[] repetidos = new int[27];
        for (int i = 0; i<repetidos.length;i++) {
            repetidos[i]= (letraVista(l[i], raiz, 0));
        }
        String root = this.mayor(repetidos);
        return root;        
    }
    
    private String mayor(int[] repetidos){
        int mayor=0;
        String masRepetidos="";
        for(int i=0;i<repetidos.length;i++){
            if(repetidos[i] > mayor){
                mayor = repetidos[i];
            }
        }
        for(int i=0; i<repetidos.length;i++){
            if(repetidos[i] == mayor){
                masRepetidos += " - "+l[i];
            }
        }
        return masRepetidos;
    }

    private int letraVista(String palabra, TrieNode nodo, int veces) {
        if (raiz.equals(nodo.getInfo())) {
            ListaCD<TrieNode> hijos = nodo.getHijos();
            for (TrieNode h : hijos) {
                veces = this.letraVista(palabra, h, veces);
            }
        } else {
            if (nodo.getInfo().equals(palabra)) {
                veces++;
            }
            ListaCD<TrieNode> hijos = nodo.getHijos();
            for (TrieNode h : hijos) {
                veces = this.letraVista(palabra, h, veces);
            }
        }
        return veces;
    }

    public TrieNode getRaiz() {
        return raiz;
    }
}
