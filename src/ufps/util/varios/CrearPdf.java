/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.varios;

import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;
import java.io.File;

/**
 *
 * @author Harold
 */
public class CrearPdf {

    public CrearPdf() {
    }

    /**
     * Método que crea el documento Pdf con la imagen recibida.
     *
     *
     * @param img
     * @param dest
     */
    public void crear(String img, String dest) {
        try {
            PdfWriter writer = new PdfWriter(dest);
            PdfDocument pdf = new PdfDocument(writer);
            Document doc = new Document(pdf);
            Image arbol = new Image(ImageDataFactory.create(img));
            doc.add(arbol);
            doc.close();
            File file = new File(img);
            file.delete();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }
    
    /**
     * Crea un pdf con la palabra buscada y un texto diciendo que encontró la palabra.
     */
    public void crearBusqueda(String img, String dest, String palabra) {
        try {
            palabra = palabra.toUpperCase();
            PdfWriter writer = new PdfWriter(dest);
            PdfDocument pdf = new PdfDocument(writer);
            Document doc = new Document(pdf);
            Image arbol = new Image(ImageDataFactory.create(img));
            doc.add(arbol);
            doc.add(new Paragraph("Se busco la palabra " + palabra).setTextAlignment(TextAlignment.CENTER));
            doc.close();
            File file = new File(img);
            file.delete();
        } catch (Exception ex) {
            ex.getMessage();
        }
    }
    
    
    /**
     * Crea un pdf de letras, afirmando que la palabra no se encuentra.
     */
    public void crearNoEsta(String dest, String palabra) {
        try {
            palabra = palabra.toUpperCase();
            PdfWriter writer = new PdfWriter(dest);
            PdfDocument pdf = new PdfDocument(writer);
            Document doc = new Document(pdf);
            doc.add(new Paragraph("La palabra " + palabra + " no se encontro").setTextAlignment(TextAlignment.CENTER));
            doc.close();
        } catch (Exception e) {
            e.getMessage();
        }
    }
}
