/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.varios;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import javax.imageio.ImageIO;
import ufps.util.colecciones_seed.ListaCD;
import ufps.util.colecciones_seed.Trie;
import ufps.util.colecciones_seed.TrieNode;

/**
 *
 * @author Harold
 */
public class ArbolGrafico {

    private int radio = 20;
    private int espacioY = 60;
    private Trie arbol;
    private HashMap<TrieNode, Point> coordenadas;
    private int width = 792, heigth = 600;

    /**
     * Constructor de la clase ArbolGráfico
     *
     * @param arbol
     */
    public ArbolGrafico(Trie arbol) {
        this.arbol = arbol;
        this.coordenadas = new HashMap<>();
    }

    /**
     * Método para guardar la imagen del árbol Trie
     * @return Nombre del archivo guardado.
     */
    public void save() {
        BufferedImage bufferedImage = new BufferedImage(width, heigth, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = bufferedImage.createGraphics();
        dibujar(g, arbol.getRaiz(), width / 2, 20, width / 2);
        File file = null;
        try {
            g.dispose();
            file = new File("arbol.png");
            ImageIO.write(bufferedImage, "png", file);
        } catch (IOException ex) {
            ex.getMessage();
        }        
    }

    /**
     * Método para guardar la imagen del árbol Trie, con una búsqueda.
     *
     * @param buscar
     */
    public void save(String buscar) {
        BufferedImage bufferedImage = new BufferedImage(width, heigth, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = bufferedImage.createGraphics();
        if (!arbol.getRaiz().getHijos().esVacia()) {
            buscar = buscar.toUpperCase();
            ListaCD<String> p = this.dividir(buscar);
            dibujar(g, arbol.getRaiz(), width / 2, 20, width / 2, p);
        }
        try {
            g.dispose();
            File file = new File("busqueda.png");
            ImageIO.write(bufferedImage, "png", file);
        } catch (IOException ex) {
            ex.getMessage();
        }
    }

    /**
     * Método para dibujar los nodos y su información en una imagen png.
     *
     * @param g, raiz, x, y, espacioX
     */
    private void dibujar(Graphics g, TrieNode raiz, int x, int y, int espacioX) {
        g.setColor(Color.GREEN);
        g.drawOval(x - radio, y, 2 * radio, 2 * radio);
        if (!raiz.getInfo().equals("")) {
            g.setColor(Color.BLACK);
            g.drawString(raiz.getInfo(), x - 5, y + 22);
        }
        ListaCD<TrieNode> hijos = raiz.getHijos();
        coordenadas.put(raiz, new Point(x, y));
        espacioX /= (hijos.getTamanio() != 0) ? hijos.getTamanio() : 1;
        for (TrieNode h : hijos) {
            g.setColor(Color.GREEN);
            Point punto = coordenadas.get(h.getPadre());
            if (hijos.getTamanio() == 1) {
                dibujarLinea(g, x, y + espacioY, punto.x, punto.y);
                dibujar(g, h, x, y + espacioY, espacioX);
            } else {
                dibujarLinea(g, x - (hijos.getTamanio() - 1) * espacioX, y + espacioY, punto.x, punto.y);
                dibujar(g, h, x - (hijos.getTamanio() - 1) * espacioX, y + espacioY, espacioX);
                x += 2 * espacioX;
            }
        }
    }

    /**
     * Método para dibujar los nodos y su información en una imagen png. Resalta
     * los que coincidan con la búsqueda.
     *
     * @param g, raiz, x, y, espacioX, buscar
     */
    private void dibujar(Graphics g, TrieNode raiz, int x, int y, int espacioX, ListaCD<String> buscar) {
        int i = 0;
        g.setColor(Color.GREEN);
        if (raiz.getInfo().equals("")) {
            g.drawOval(x - radio, y, 2 * radio, 2 * radio);
        } else {
            if (buscar.esVacia()) {
                g.drawOval(x - radio, y, 2 * radio, 2 * radio);
            } else {
                if (buscar.get(i).equals(raiz.getInfo())) {
                    g.fillOval(x - radio, y, 2 * radio, 2 * radio);
                    buscar.eliminar(i);
                } else {
                    g.drawOval(x - radio, y, 2 * radio, 2 * radio);
                }
            }
            g.setColor(Color.BLACK);
            g.drawString(raiz.getInfo(), x - 5, y + 22);
        }
        ListaCD<TrieNode> hijos = raiz.getHijos();
        coordenadas.put(raiz, new Point(x, y));
        espacioX /= (hijos.getTamanio() != 0) ? hijos.getTamanio() : 1;
        for (TrieNode h : hijos) {
            g.setColor(Color.GREEN);
            Point punto = coordenadas.get(h.getPadre());
            if (hijos.getTamanio() == 1) {
                dibujarLinea(g, x, y + espacioY, punto.x, punto.y);
                dibujar(g, h, x, y + espacioY, espacioX, buscar);
            } else {
                dibujarLinea(g, x - (hijos.getTamanio() - 1) * espacioX, y + espacioY, punto.x, punto.y);
                dibujar(g, h, x - (hijos.getTamanio() - 1) * espacioX, y + espacioY, espacioX, buscar);
                x += 2 * espacioX;
            }
        }
    }

    /**
     * Método que dibuja las líneas del nodo padre al nodo hijo. Recibe las
     * coordenadas del nodo padre y el nodo hijo.
     *
     * @param g, x1, y1, x2, y2
     */
    private void dibujarLinea(Graphics g, int x1, int y1, int x2, int y2) {
        g.drawLine(x2, y2 + 2 * radio, x1, y1);
    }

    /**
     * Método que divide una cadena de texto. Retorna una ListaCD
     */
    private ListaCD<String> dividir(String palabra) {
        ListaCD<String> palabraDividida = new ListaCD<>();
        String[] split = palabra.split("");
        for (int i = 0; i < split.length; i++) {
            if (i < split.length - 2) {
                String digrafo = split[i] + split[i + 1];
                if (esDigrafo(digrafo, split[i + 2])) {
                    palabraDividida.insertarAlFinal(digrafo);
                    i++;
                } else {
                    palabraDividida.insertarAlFinal(split[i]);
                }
            } else {
                palabraDividida.insertarAlFinal(split[i]);
            }
        }
        return palabraDividida;
    }

    /**
     * Método que determina si dos letras son dígrafo. Retorna true si lo
     * cumple, de lo contrario false.
     */
    private boolean esDigrafo(String palabra, String vocal) {
        if (vocal.equals("E") || vocal.equals("I")) {
            if (palabra.equals("QU") || palabra.equals("GU")) {
                return true;
            }
        }
        return palabra.equals("CH") || palabra.equals("LL") || palabra.equals("RR");
    }

    /**
     * Método que reemplaza las tildes en una palabra. Retorna el string sin
     * tildes.
     */
    private String tildes(String palabra) {
        String[] split = palabra.split("");
        palabra = "";
        for (int i = 0; i < split.length; i++) {
            switch (split[i]) {
                case "Á":
                    split[i] = "A";
                    break;
                case "É":
                    split[i] = "E";
                    break;
                case "Í":
                    split[i] = "I";
                    break;
                case "Ó":
                    split[i] = "O";
                    break;
                case "Ú":
                    split[i] = "U";
                    break;
            }
            palabra += split[i];
        }
        return palabra;
    }
}
